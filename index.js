const express = require("express");
const app = express();

app.get("/", (req, res) => {
  res.send("<h1>Welcome to our one and only app!! v2 baby</h1>");
});

app.get("/add/:num1/:num2", (req, res) => {
  const { num1, num2 } = req.params;
  res.send(`Answer : ${addNumbers(Number(num1), Number(num2))}`);
});

const PORT = process.env.PORT || 3000;

app.listen(PORT, () => {
  console.log("App started!");
});

function addNumbers(num1, num2) {
  return num1 + num2;
}

module.exports = { addNumbers };
