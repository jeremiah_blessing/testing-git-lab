var addNumber = require("../index");
var expect = require("chai").expect;

describe("AddNumber functions", () => {
  it("Should return the addition", () => {
    expect(addNumber.addNumbers(1, 2)).to.be.equal(3);
  });
});
